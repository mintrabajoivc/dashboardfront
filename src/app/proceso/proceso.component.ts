import { Component, OnInit } from '@angular/core';
import { Sancion } from '../model/sancion';
import { ExcelService } from '../services/excel.service';
import { SancionService } from '../services/sancion.service';

@Component({
  selector: 'app-proceso',
  templateUrl: './proceso.component.html',
  styleUrls: ['./proceso.component.css']
})
export class ProcesoComponent implements OnInit {


  consulta : number = 1;
  especieX : number = 1;
  sancionList: Sancion[] = [];


  data: Array<any> =  [
    { name: 'Moran', role: 'back' },
    { name: 'Alain', role: 'front' },
    { name: 'Tony', role: 'back' },
    { name: 'Mike', role: 'back' },
    { name: 'Abo', role: 'back' },
    { name: 'Toni', role: 'back' },
  ]


  constructor(private excelService: ExcelService,
    private sancionService: SancionService ) { }

  ngOnInit() {
    console.log(this.especieX);
  }

  exportAsXLSX():void {
    this.excelService.exportAsExcelFile(this.data, 'sample');
  }

  consultaSancion() {
    this.consulta = 1;
    
  }

  totalSancion() {
    this.consulta = 2;
 //   this.sancionService.getSancionNumMeses(-4).subscribe(x => {this.sancionList = x; console.log(x)});
  }
  estadoProceso() {    
    this.consulta = 3;
 //   this.sancionService.getSancionNumMeses(-4).subscribe(x => {this.sancionList = x; console.log(x)});
  }

}
