import { Component, OnInit } from '@angular/core';
import { Sancion } from '../model/sancion';
import { ExcelService } from '../services/excel.service';
import { SancionService } from '../services/sancion.service';

@Component({
  selector: 'app-consTotSancion',
  templateUrl: './consTotSancion.component.html',
  styleUrls: ['./proceso.component.css']
})
export class ConsTotSancComponent implements OnInit {


  consulta : number = 1;
  especieX : number = 1;
  sancionList: Sancion[] = [];


  data: Array<any> =  [
    { name: 'Moran', role: 'back' },
    { name: 'Alain', role: 'front' },
    { name: 'Tony', role: 'back' },
    { name: 'Mike', role: 'back' },
    { name: 'Abo', role: 'back' },
    { name: 'Toni', role: 'back' },
  ]


  constructor(private excelService: ExcelService,
    private sancionService: SancionService ) { }

  ngOnInit() {
    console.log(this.especieX);
    this.totalSancion();
  }

  exportAsXLSX():void {
    this.excelService.exportAsExcelFile(this.data, 'sample');
  }

  consultaSancion() {
    console.log("____________________ ");
  }

  totalSancion() {
    console.log("xxxxxxxxxxxxxxxxxxx ");
    this.consulta = 2;
    this.sancionService.getSancionNumMeses(-4).subscribe(x => {this.sancionList = x; console.log(x)});
  }

}
