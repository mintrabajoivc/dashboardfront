import { Component, OnInit } from '@angular/core';
import { Sancion } from '../model/sancion';
import { EstProcService } from '../services/estProc.service';
import { ExcelService } from '../services/excel.service';
import { SancionService } from '../services/sancion.service';

@Component({
  selector: 'app-consEstProceso',
  templateUrl: './consEstProceso.component.html',
  styleUrls: ['./proceso.component.css']
})
export class ConsEstProcComponent implements OnInit {


  consulta : number = 1;
  especieX : number = 1;
  sancionList: Sancion[] = [];


  data: Array<any> =  [
    { name: 'Moran', role: 'back' },
    { name: 'Alain', role: 'front' },
    { name: 'Tony', role: 'back' },
    { name: 'Mike', role: 'back' },
    { name: 'Abo', role: 'back' },
    { name: 'Toni', role: 'back' },
  ]


  constructor(private excelService: ExcelService,
    private estProcService: EstProcService,
     ) { }

  ngOnInit() {
    console.log(this.especieX);
    this.totalEstadoProceso();
  }

  exportAsXLSX():void {
    this.excelService.exportAsExcelFile(this.data, 'sample');
  }

  consultaSancion() {
    console.log("____________________ ");
  }

  totalEstadoProceso() {
    console.log("xxxxxxxxxxxxxxxxxxx ");
    this.consulta = 3;
    this.estProcService.getEstadoProceso(15691, 'ACTIVO',-4).subscribe(x => {this.sancionList = x; console.log(x)});

  }

}
