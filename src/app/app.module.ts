import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProcesoComponent } from './proceso/proceso.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSliderModule } from '@angular/material/slider';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import {  MatNativeDateModule} from '@angular/material';
import { FormsModule } from '@angular/forms';
import {MatCardModule} from '@angular/material/card';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { ConsTotSancComponent } from './proceso/consTotSancion.component';
import { ConsEstProcComponent } from './proceso/consEstProceso.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import {DashboardService} from  './services/dashboard.service';
import { ChartsModule, ThemeService } from 'ng2-charts';
@NgModule({
  declarations: [
    AppComponent,
    ProcesoComponent,
    ConsTotSancComponent,
    ConsEstProcComponent,
    DashboardComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSliderModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatNativeDateModule,
    FormsModule,
    MatCardModule,
    HttpClientModule,
    ScrollingModule,
    ChartsModule,
  ],
  providers: [DashboardService,ThemeService,],
  bootstrap: [AppComponent]
})
export class AppModule { }
