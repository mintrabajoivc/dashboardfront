export class Sancion {
    idDirTerritorial ?: number;
    nombreDirTerritorial  ?: string;
    estadoProceso  ?: string;
    cantidad  ?: number;
    valorSanciones  ?: number;
}