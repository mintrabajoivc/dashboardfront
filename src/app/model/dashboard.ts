export class Dashboard{
  activos: number;
	finalizados: number;
	total: number;
	despacho: number;
	ivc: number;
	rcc: number;
	ivcRcc: number;
	totalDespacho: number;
	expedientesConCaducidad: number;
	expedientesVencenSemana: number;
	expedientesVencenMes: number;
	expedientesVencenTrimestre: number;
}
