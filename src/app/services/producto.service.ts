import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';



@Injectable({
  providedIn: 'root'
})
export class SampleService {

  samplesChange = new BehaviorSubject<string>('');
  private url = `${environment.apiURL}historia1/`;
  constructor(private http: HttpClient) {
  }
  

}
