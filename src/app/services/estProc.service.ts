
 import { Injectable } from '@angular/core';
 import { environment } from '../../environments/environment';
 import { HttpClient, HttpParams } from '@angular/common/http';
 import { BehaviorSubject, Observable } from 'rxjs';
import { Sancion } from '../model/sancion';

 
 @Injectable({
   providedIn: 'root'
 })
 export class EstProcService {
 
   sancionChange = new BehaviorSubject<Sancion>(new Sancion());
   private url = `${environment.apiURL}proceso/`;
      

   constructor(private http: HttpClient) {
   } 
 
   getEstadoProceso( idFlujoPk: number, estado: string, numMeses: number ): Observable<Sancion[]> {        
     return this.http.get<Sancion[]>(`${this.url}estProcesoDt`, {
       params: new HttpParams()
       .set('idFlujoPk', idFlujoPk.toString())       
       .set('estado', estado.toString())    
       .set('numMeses', numMeses.toString())      
     });
   }    
 }  