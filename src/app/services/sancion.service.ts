
 import { Injectable } from '@angular/core';
 import { environment } from '../../environments/environment';
 import { HttpClient, HttpParams } from '@angular/common/http';
 import { BehaviorSubject, Observable } from 'rxjs';
import { Sancion } from '../model/sancion';

 
 @Injectable({
   providedIn: 'root'
 })
 export class SancionService {
 
   sancionChange = new BehaviorSubject<Sancion>(new Sancion());
   private url = `${environment.apiURL}proceso/`;
      

   constructor(private http: HttpClient) {
   } 
 
   getSancionNumMeses( numMeses: number): Observable<Sancion[]> {        
     return this.http.get<Sancion[]>(`${this.url}totalSancionDt`, {
       params: new HttpParams()
       .set('numMeses', numMeses.toString())       
     });
   }    
 }  