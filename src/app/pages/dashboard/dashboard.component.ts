import { Component, OnInit } from '@angular/core';
import { Dashboard } from '../../model/dashboard';
import { DashboardService } from '../../services/dashboard.service';
import { ChartType, ChartOptions, ChartDataSets,  RadialChartOptions } from 'chart.js';
import { SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip, MultiDataSet } from 'ng2-charts';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  public pieChartOptions: ChartOptions = {
    responsive: true,
  };
  public pieChartLabels: Label[];
  public pieChartData: SingleDataSet;
  public pieChartType: ChartType = 'doughnut';
  public pieChartLegend = true;
  public pieChartPlugins = [];
  public doughnutChartLabels: Label[];
  public doughnutChartData: MultiDataSet ;
  public doughnutChartType: ChartType = 'doughnut';
  public barChartOptions: ChartOptions = {
    responsive: true,
   };
   public barChartLabels: Label[] = ['Semanal', 'Mensual', 'Trimestral'];
   public barChartType: ChartType = 'bar';
   public barChartLegend = true;
   public barChartPlugins = [];

   public barChartData: ChartDataSets[];
  public ready = false;
  constructor(private dashboardService: DashboardService) { }
  public dashboard: Dashboard;
  ngOnInit() {
    this.consultarDashboard();
  }

  private consultarDashboard(){
    this.dashboardService.getDashboardInit().subscribe((res: Response) =>{
      const response: any = res;
      this.dashboard = response;
      this.pieChartLabels = ['Finalizados ['+response.finalizados+']', 'Activos ['+response.activos+']'];
      this.pieChartData = [response.finalizados, response.activos];
      monkeyPatchChartJsTooltip();
      monkeyPatchChartJsLegend();
      this.doughnutChartData = [
        [this.dashboard.ivc, this.dashboard.rcc, this.dashboard.ivcRcc]
      ];
      this.barChartData = [
        { data: [this.dashboard.expedientesVencenMes, this.dashboard.expedientesVencenSemana, this.dashboard.expedientesVencenTrimestre], label: 'Expedientes a vencer' }
      ];
     this.doughnutChartLabels = ['IVC ['+this.dashboard.ivc+']', 'RCC ['+this.dashboard.rcc+']', 'IVC-RCC ['+this.dashboard.ivcRcc+']'];
      this.ready = true;

    }, (error: any)=>{
      console.log('DashboardREsponse '+JSON.stringify(error));
    });
  }

}
